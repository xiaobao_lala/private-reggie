package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.CustomException;
import com.reggie.common.R;
import com.reggie.dto.DishDto;
import com.reggie.entity.*;
import com.reggie.service.CategoryService;
import com.reggie.service.DishFlavorService;
import com.reggie.service.DishService;
import com.reggie.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SetmealDishService setmealDishService;

    /**
     * 新增菜品成功
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto) {
        log.info("新增菜品数据：{}", dishDto);
        dishService.saveWithFlavor(dishDto);
        return R.success("新增菜品成功");
    }

    /**
     * 分页查询
     */
    @GetMapping("/page")
    public R<Page> page(Integer page, Integer pageSize, String name) {
        log.info("菜品查询，参数{}, {}", page, pageSize);
        //构造分页构造器
        Page<Dish> pageInfo = new Page<>(page, pageSize);
        Page<DishDto> dishDtoPage = new Page<>();

        //添加构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加过滤器
        queryWrapper.like(name != null, Dish::getName, name);
        //添加排序
        queryWrapper.orderByDesc(Dish::getUpdateTime);

        //执行分页查询
        dishService.page(pageInfo, queryWrapper);

        //对象拷贝
        BeanUtils.copyProperties(pageInfo, dishDtoPage, "records");
        List<Dish> records = pageInfo.getRecords();
        List<DishDto> list = records.stream().map((item) -> {

            DishDto dishDto = new DishDto();
            //对象拷贝
            BeanUtils.copyProperties(item, dishDto);
            //获取各自分类id
            Long categoryId = item.getCategoryId();
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            //设置菜品名
            if (category != null) {
                dishDto.setCategoryName(category.getName());
            }

            return dishDto;
        }).collect(Collectors.toList());

        dishDtoPage.setRecords(list);

        return R.success(dishDtoPage);
    }

    /**
     * 根据id查询数据
     */
    @GetMapping("/{id}")
    public R<DishDto> getById(@PathVariable Long id) {
        DishDto dishDto = dishService.getByIdWithFlavor(id);
        return R.success(dishDto);
    }

    /**
     * 更新菜品成功
     */
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto) {
        log.info("更新菜品数据：{}", dishDto);
        dishService.updateWithFlavor(dishDto);
        return R.success("更新菜品成功");
    }

    /**
     * 根据条件查询菜品数据
     */
    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish) {
        //构造条件查询对象
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(dish.getCategoryId() != null, Dish::getCategoryId, dish.getCategoryId());
        //添加查询条件 状态为1 起售
        queryWrapper.eq(Dish::getStatus, 1);

        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> list = dishService.list(queryWrapper);
        List<DishDto> dishDtoList = list.stream().map((item) -> {

            DishDto dishDto = new DishDto();
            //对象拷贝
            BeanUtils.copyProperties(item, dishDto);
            //获取各自分类id
            Long categoryId = item.getCategoryId();
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);

            //设置菜品名
            if (category != null) {
                dishDto.setCategoryName(category.getName());
            }

            //获取菜品id
            Long dishId = item.getId();
            LambdaQueryWrapper<DishFlavor> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(DishFlavor::getDishId, dishId);
            List<DishFlavor> dishFlavorList = dishFlavorService.list(lambdaQueryWrapper);
            dishDto.setFlavors(dishFlavorList);
            return dishDto;
        }).collect(Collectors.toList());
        return R.success(dishDtoList);
    }

    /**
     * 删除菜品
     */
    @DeleteMapping
    //请求参数
    public R<String> delete(@RequestParam List<Long> ids) {
        log.info("删除菜品 ids:{}", ids);
        dishService.removeWithFlavor(ids);
        return R.success("删除成功");
    }

    /**
     * 修改菜品状态
     */
    @PostMapping("/status/{status}")
    public R<String> statusUpdate(@PathVariable Integer status, @RequestParam List<Long> ids) {
        log.info("根据id修改状态 status:{}, ids:{}", status, ids);
        //总结：1多看方法 用好积木 节约时间 提高代码质量

        //停售 变 启用 ； 启用 变 禁用
        //方案1 统一调度 不能根据特定字段获取值 需要动态sql  LambdaQueryWrapper.in（动态sql）
        //判断能否统一更改
        //查询关联表 有数据 则菜品中存在关联数据 不能停售
        //不能，抛出业务 停售异常
        if (status == 0) {
            //查询
            LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
            //in方法
            queryWrapper.in(SetmealDish::getDishId, ids);
            //统计方法
            int count = setmealDishService.count(queryWrapper);
            if (count > 0) {
                //存在套餐关联 不能停售 抛出异常
                throw new CustomException("有菜品关联了套餐信息，不能停售");
            }
        }
        //1根据ids 获取list listByIds()
        List<Dish> dishlList = dishService.listByIds(ids);
        //2统一修改status lambda表达式 匿名函数
        dishlList = dishlList.stream().map((item) -> {
            item.setStatus(status);
            return item;
        }).collect(Collectors.toList());
        //3执行setmealList更新操作 updateBatchById
        dishService.updateBatchById(dishlList);
        return R.success("状态修改成功");
    }
}
/**
 * 根据条件查询菜品数据
 */
    /*@GetMapping("/list")
    public R<List<Dish>> list(Dish dish) {
        //构造条件查询对象
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(dish.getCategoryId() != null, Dish::getCategoryId, dish.getCategoryId());
        //添加查询条件 状态为1 起售
        queryWrapper.eq(Dish::getStatus, 1);

        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> list = dishService.list(queryWrapper);

        return R.success(list);
    }*/
