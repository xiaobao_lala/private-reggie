package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.R;
import com.reggie.entity.Employee;
import com.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /*
     *   员工登录请求
     * */
    @PostMapping("login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {

//        1、将页面提交的密码password进行md5加密处理
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());

//        2、根据页面提交的用户名username查询数据库
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(employee::getUsername, employee.getUsername());
        Employee emp = employeeService.getOne(queryWrapper);

//         3、如果没有查询到则返回登录失败结果
        if (emp == null) {
            return R.error("登录失败");
        }
//        4、密码比对，如果不一致则返回登录失败结果
        if (!emp.getPassword().equals(password)) {
            return R.error("登录失败");
        }
//        5、查看员工状态，如果为己禁用状态，则返回员工已禁用结果
        if (emp.getStatus() == 0) { // 0禁用 1可用
            return R.error("账号已禁用f");
        }

//        6、登录成功，将员工id存入Session并返回登录成功结果 Session 名称
        request.getSession().setAttribute("employee", emp.getId());
        return R.success(emp);
    }

    /*
     *   员工登出
     * */
    @PostMapping("logout")
    public R<String> logout(HttpServletRequest request) {
        // 清理session中保存的员工id
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");
    }

    /*
     *   储存员工信息
     * */
    @PostMapping
    public R<String> save(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("新增员工{}", employee.toString());
        //补全员工信息
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));

        //employee.setCreateTime(LocalDateTime.now());
        //employee.setUpdateTime(LocalDateTime.now());

        //获取用户当前id
        Long empId = (Long) request.getSession().getAttribute("employee");

        //employee.setCreateUser(empId);
        //employee.setUpdateUser(empId);

        //用封装好的方法 就只需完成对象补全
        employeeService.save(employee);

        return R.success("新增员工成功");
    }

    /*
     *   员工页面查询
     * */
    @GetMapping("/page")
    public R<Page<Employee>> page(Integer page, Integer pageSize, String name) {
        log.info("员工分页查询： page:{}, pageSize:{}, name:{}", page, pageSize, name);
        //构造分页构造器
        Page<Employee> pageInfo = new Page<>(page, pageSize);
        //构造条件构造器
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        //添加一个条件
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name);
        //添加排序条件
        queryWrapper.orderByDesc(Employee::getUpdateTime);

        //执行查询 SM层封装 省代码
        employeeService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }

    /*
     *   根据id修改员工信息
     * */
    @PutMapping
    public R<String> update(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("更新员工信息{}", employee.toString());
        Long empId = (Long) request.getSession().getAttribute("employee");
        employee.setUpdateTime(LocalDateTime.now());
        employee.setUpdateUser(empId);
        employeeService.updateById(employee);
        return R.success("员工信息修改成功");
    }

    /*
     *   根据id查询员工
     * */
    @GetMapping("{id}")
    public R<Employee> getById(@PathVariable Integer id) {
        log.info("根据id查询员工，id:{}", id);
        Employee emp = employeeService.getById(id);
        return R.success(emp);
    }
}
