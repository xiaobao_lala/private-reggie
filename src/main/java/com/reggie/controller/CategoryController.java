package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.R;
import com.reggie.entity.Category;
import com.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/*
 *   分类管理
 * */
@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /*
     *   新增分类
     * */
    @PostMapping
    public R<String> save(@RequestBody Category category) {
        log.info("新增分类, {}", category);
        categoryService.save(category);
        return R.success("新增分类成功");
    }

    /*
     *  分类分页查询
     * */
    @GetMapping("/page")
    public R<Page> page(Integer page, Integer pageSize) {
//        log.info("分页查询，参数"+page+" "+pageSize);
        // 分页构造器
        Page<Category> pageInfo = new Page<>(page, pageSize);
        // 条件构造器
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        //添加排序条件 根据sort排序 升序
        queryWrapper.orderByAsc(Category::getSort);

        //进行分页查询
        categoryService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    /*
     *   根据id删除分类信息
     * */
    @DeleteMapping
    public R<String> delete(Long ids) {
        log.info("删除分类，id：{}", ids);
//        categoryService.removeById(id);
        categoryService.remove(ids);
        return R.success("分类信息删除成功");
    }

    /*
     *   根据id 修改数据
     * */
    @PutMapping
    public R<String> update(@RequestBody Category category) {
        log.info("根据id修改数据，信息:{}", category);
        categoryService.updateById(category);
        return R.success("修改分类成功");
    }

    /*
     *   根据条件查询分类数据
     *   查询菜品分类
     * */
    @GetMapping("/list")
    public R<List<Category>> list(Category category) {
        //条件构造器
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        //添加条件
        queryWrapper.eq(category.getType() != null, Category::getType, category.getType());
        //添加排序条件
        queryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);

        List<Category> list = categoryService.list(queryWrapper);
        return R.success(list);
    }
}
