package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.R;
import com.reggie.dto.SetmealDto;
import com.reggie.entity.Category;
import com.reggie.entity.Setmeal;
import com.reggie.service.CategoryService;
import com.reggie.service.SetmealDishService;
import com.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SetmealDishService setmealDishService;

    /**
     * 新增套餐
     */
    @PostMapping
    //获取请求体
    public R<String> save(@RequestBody SetmealDto setmealDto) {
        log.info("新增套餐：{}", setmealDto);
        setmealService.saveWithDish(setmealDto);
        return R.success("新增套餐成功");
    }

    /**
     * 套餐分页查询
     */
    @GetMapping("/page")
    public R<Page> pagw(Integer page, Integer pageSize, String name) {
        //构造分页构造器
        Page<Setmeal> pageInfo = new Page<>(page, pageSize);
        Page<SetmealDto> dtoPage = new Page<>();
        //条件
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //模糊查询 前提条件 name != null 匹配类型 Setmeal.name 内容：name
        queryWrapper.like(name != null, Setmeal::getName, name);
        //排序
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        //执行查询操作
        setmealService.page(pageInfo, queryWrapper);
        //page拷贝 泛型不一不用拷贝
        BeanUtils.copyProperties(pageInfo, dtoPage, "records");
        List<Setmeal> records = pageInfo.getRecords();
        //item = Setmeal
        List<SetmealDto> list = records.stream().map((item) -> {
            SetmealDto setmealDto = new SetmealDto();
            //对象拷贝
            BeanUtils.copyProperties(item, setmealDto);
            //分类id
            Long categoryId = item.getCategoryId();
            //根据分类id查询分类对象
            Category category = categoryService.getById(categoryId);
            if (category != null) {
                //分类名称
                String categoryName = category.getName();
                setmealDto.setCategoryName(categoryName);
            }
            return setmealDto;
        }).collect(Collectors.toList());
        dtoPage.setRecords(list);
        return R.success(dtoPage);
    }

    /**
     * 套餐删除
     */
    @DeleteMapping
    //请求参数
    public R<String> delete(@RequestParam List<Long> ids) {
        log.info("删除套餐 ids:{}", ids);
        setmealService.removeWithDish(ids);
        return R.success("删除成功");
    }

    /**
     * 根据id修改状态 0禁用 1启用
     */
    @PostMapping("/status/{status}")
    public R<String> statusUpdate(@PathVariable Integer status, @RequestParam List<Long> ids) {
        log.info("根据id修改状态 status:{}, ids:{}", status, ids);
        //总结：1多看方法 用好积木 节约时间 提高代码质量

        //停售 变 启用 ； 启用 变 禁用

        //方案1
        //目的更新 status数据
        //描述 根据id修改值
        //只需要sql Mapper进行修改就行 直接不用判断 需引入新的依赖 放弃

        //方案2
        // 根据id获取setmel类 查询 都需要动态sql
        //查询操作  1构造器 2加条件 3操作
        //创建条件构造器
        //添加条件
        //执行查询操作
        //修改setmeal的 status
        //执行更新操作

        //方案3
        //固定sql 效率虽低下 但能完成功能 反复的io操作 多次调用

        //方案4
        //1获取status = 0的list 查询
        //查询操作  1构造器 2加条件 3操作 通用操作 统一模板
        //2根据id 新建待修改数据list
        //3执行setmealList更新操作 updateBatchById()

        //方案5
        //1根据ids 获取list listByIds()
        List<Setmeal> setmealList = setmealService.listByIds(ids);
        //2统一修改status lambda表达式 匿名函数
        setmealList = setmealList.stream().map((item) -> {
            item.setStatus(status);
            return item;
        }).collect(Collectors.toList());
        //3执行setmealList更新操作 updateBatchById
        setmealService.updateBatchById(setmealList);

        //总结：1多看方法 用好积木 节约时间 提高代码质量
        return R.success("状态修改成功");
    }

    /**
     * 套餐数据回显，根据id回显
     */
    @GetMapping("/{id}")
    public R<SetmealDto> getById(@PathVariable Long id) {
        log.info("套餐数据回显id：{}", id);
        SetmealDto setmealDto = setmealService.getByIdToDto(id);
        return R.success(setmealDto);
    }

    /**
     * 根据菜品dto 更新菜品数据
     */
    @PutMapping
    public R<String> updateFromDto(@RequestBody SetmealDto setmealDto) {
        log.info("根据菜品dto 更新菜品数据 菜品Dto：{}", setmealDto);
        setmealService.updateFromDto(setmealDto);
        return R.success("修改套餐成功");
    }

    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId() != null, Setmeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus() != null, Setmeal::getStatus, setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> list = setmealService.list(queryWrapper);
        return R.success(list);
    }

}
