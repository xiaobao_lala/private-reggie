package com.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.reggie.common.CustomException;
import com.reggie.dto.SetmealDto;
import com.reggie.entity.Category;
import com.reggie.entity.DishFlavor;
import com.reggie.entity.Setmeal;
import com.reggie.entity.SetmealDish;
import com.reggie.mapper.SetmealMapper;
import com.reggie.service.CategoryService;
import com.reggie.service.SetmealDishService;
import com.reggie.service.SetmealService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private CategoryService categoryService;

    @Override
    //事务注解 开启事务
    @Transactional
    public void saveWithDish(SetmealDto setmealDto) {
        //保存套餐基本信息
        this.save(setmealDto);

        //保存套餐和菜品的关联信息
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        setmealDishes = setmealDishes.stream().map((item) -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(setmealDishes);


    }

    /**
     * 删除套餐及其关联表中的数据
     */
    @Override
    @Transactional
    public void removeWithDish(List<Long> ids) {
        //ids列表中 只要有一个不能删除都会进行事务回滚

        //目的sql select count(*) from setmeal where id in (1,2,3) and status = 1

        //1查询状态是否可删 结果售卖中
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //类型与值
        queryWrapper.in(Setmeal::getId, ids);
        queryWrapper.eq(Setmeal::getStatus, 1);

        int count = this.count(queryWrapper);
        if (count > 0) {
            //如果不能删除 抛出业务异常
            //售卖中 不能删除
            throw new CustomException("售卖中,不能删除");

        }

        //2产出套餐表中的数据
        this.removeByIds(ids);

        //3删除关系表中的数据
        //sql delete from setmeal_dish where setmeal_id in (1,2,3)
        LambdaQueryWrapper<SetmealDish> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.in(SetmealDish::getSetmealId, ids);

        setmealDishService.remove(lambdaQueryWrapper);
    }

    /**
     * 数据回显
     * 查询数据
     * id：Setmeal的id（适当标注）
     */
    @Override
    public SetmealDto getByIdToDto(Long id) {
        Setmeal setmeal = this.getById(id);
        SetmealDto setmealDto = new SetmealDto();
        //获取基础信息setmeal
        BeanUtils.copyProperties(setmeal, setmealDto);
        //获取category name
        Long categoryId = setmeal.getCategoryId();
        Category category = categoryService.getById(categoryId);
        //null ---> 套餐没有绑定分类
        if (category != null) {
            //添加到Dto中
            setmealDto.setCategoryName(category.getName());
        }
        //获取List<SetmealDish>
        //查询关联的id 返回list
        //查询 模板 1添加构造器 2添加条件 3执行
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> setmealDishList = setmealDishService.list(queryWrapper);
        //添加到Dto中
        setmealDto.setSetmealDishes(setmealDishList);
        return setmealDto;
    }

    @Override
    @Transactional
    public void updateFromDto(SetmealDto setmealDto) {
        //更新dish表的基本信息
        this.updateById(setmealDto);
        //先清理，再插入
//        LambdaQueryWrapper 搜索Query Lambda表达式 封装Wrapper 参数类
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getDishId, setmealDto.getId());

        setmealDishService.remove(queryWrapper);
        //插入新数据
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //补全setmealId
        setmealDishes = setmealDishes.stream().map((item) -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        //批量保存SetmealDish数据 关系数据
        setmealDishService.saveBatch(setmealDishes);

    }
}
