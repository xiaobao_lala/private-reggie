package com.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.dto.SetmealDto;
import com.reggie.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {
    /**
     * 保存套餐数据 同时保存菜品的关联数据
     */
    public void saveWithDish(SetmealDto setmealDto);

    /**
     * 删除套餐同时删除 关联信息
     */
    public void removeWithDish(List<Long> ids);

    /**
     * 根据id回显套餐数据
     */
    public SetmealDto getByIdToDto(Long id);

    /**
     * 根据菜品dto 更新菜品数据
     */
    public void updateFromDto(SetmealDto setmealDto);
}
