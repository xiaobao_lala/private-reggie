package com.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.entity.OrderDetail;
import com.reggie.mapper.OrderDetailMapper;

public interface OrderDetailService extends IService<OrderDetail> {
}
